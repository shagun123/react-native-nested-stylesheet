import * as React from 'react';
import {SafeAreaView, Text, View} from 'react-native';
import {createNestedStylesheet} from '../utils';

class Home extends React.Component<any, any> {
  render() {
    return (
      <SafeAreaView style={styles.mainView}>
        <View>
          <Text style={styles.text1}>This is my Home Screen</Text>
          <Text style={styles.text2}>This is my text</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles: any = createNestedStylesheet({
  intro: {
    text1: {
      color: 'blue',
    },
    text2: {
      color: 'red',
    },
  },
  mainView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Home;
