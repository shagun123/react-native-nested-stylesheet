import {StyleSheet} from 'react-native';
// {
//     intro:{
//         container:{
//
//         },
//         text:{
//
//         }
//     },
// mainView:{
// backgroundColor:'#fff'
// }
// }

export function createNestedStylesheet(styles: object) {
  let stylesheet = {};
  const keys = Object.keys(styles);
  for (let key of keys) {
    if (isNestedObject(styles[key])) {
      const nestedKeys = Object.keys(styles[key]);
      for (let item of nestedKeys) {
        stylesheet[item] = styles[key][item];
      }
    } else {
      stylesheet[key] = styles[key];
    }
  }
  console.log(stylesheet);
  return StyleSheet.create(stylesheet);
}

function isNestedObject(object: object) {
  let isNested = false;
  const keys = Object.keys(object);
  for (let key of keys) {
    if (object[key] instanceof Object) {
      isNested = true;
      break;
    }
  }
  return isNested;
}
